//Author Ricardo Sanchez 
package pilas;

import java.util.Scanner;
import java.util.Stack;

public class Pilas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Ricardo Daniel Sanchez Rosario, 63876");
        System.out.println("Primer ejercicio");
        System.out.println("");{
        String cadena = "(123*4)+((12-1)*(1+2))";
        Stack pila = new Stack();
        boolean flag = true;
        char[] acad = cadena.toCharArray();
        for(char c: acad){
        if(c =='('){
        pila.push(c);
        System.out.println("Insertando = "+c);
        }
        if(c == ')'){
        if(!pila.empty()){
        pila.pop();
        System.out.println("Extrayendo = "+c);
        } else {flag = false;}
        }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if(pila.size()==0 && flag){System.out.println("Expresión valida");} else
        {System.out.println("Expresión invalida");}
    
    }
        System.out.println("");
        System.out.println("Segundo ejercicio");
        System.out.println("");{
        String cadena = "<b><i>Hola ISC</i></b>";
        Stack pila = new Stack();
        boolean flag = true;
        char[] acad = cadena.toCharArray();
        for(char c: acad){
        if(c =='<'){
        pila.push(c);
        System.out.println("Insertando = "+c);
        }
        if(c == '>'){
        if(!pila.empty()){
        pila.pop();
        System.out.println("Extrayendo = "+c);
        } else {flag = false;}
        }
        }
        System.out.println(pila.size());
        System.out.println(flag);
        if(pila.size()==0 && flag){System.out.println("Expresión valida");} else
        {System.out.println("Expresión invalida");
    }
    }
        System.out.println("");
        Scanner entrada = new Scanner(System.in);
        String respuesta = "S";
        String cadena="";
        System.out.println("Tercer ejercicio");
        while (respuesta.equalsIgnoreCase("S")){
            System.out.println ("¿Qué cadena desea analizar?");
            cadena=entrada.nextLine();
            String textoPorPantalla="";
            char[] array = {'a','e','i','o','u'};
            for (int i=0; i<array.length; i++) {
                switch (analizarVocal(cadena,array[i])){
                    case 1: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es par. "; break;
                    case -1: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es impar. "; break;
                    case 0: textoPorPantalla=textoPorPantalla+"\nEl número de letras "+array[i]+" es cero. "; break;
                }
            }
                System.out.println (textoPorPantalla);
                System.out.print ("¿Desea analizar otra cadena? (S/N) ");
                respuesta = entrada.nextLine();           
        }
        System.out.println("");
        System.out.println("Cuarto ejercicio");
        System.out.println("");{
        int numero;
        numero =1;
        
        int [] Pila1 = new int[numero];
        int [] Pila2 = new int[numero];
        int [] Pila3 = new int[numero];
        int [] Pila4 = new int[numero];
        int [] Pila5 = new int[numero];
        int [] sumarPila = new int [numero];
        int [] restarPila = new int [numero];
        
        for(int  p = 0; p < numero; p++){}
        int p = 0;
        
        Pila1[p] = (int) (Math.random() + 10 + 15);
        System.out.println("Suma (10) , Suma(15) = " + Pila1[p]);
        
        Pila2[p] = (int) (Math.random() + 25 - 5);
        System.out.println("Suma (15) , Resta(5) = " + Pila2[p]);
        
        Pila3[p] = (int) (Math.random() +  25 + 0);
        System.out.println("Cancelar() = " + Pila3[p]);
        
        Pila4[p] = (int) (Math.random() +  20 + 15);
        System.out.println("Resta(5) , Suma(15) = " + Pila4[p]);
        
         for (int s = 0; s<numero; s++){
        sumarPila[s]=Pila1[s] + Pila2[s] + Pila3[p] + Pila4[p];
        }
        int s = 0;
        System.out.println("Total general de la pila:"  + sumarPila[s] );
     }
}

public static int analizarVocal (String cadena, char vocalParaAnalizar) {
        Stack<String> pila = new Stack<>(); char v=vocalParaAnalizar; String vocal=String.valueOf(vocalParaAnalizar);
        int i=0; int auxiliar=0;
        while (i<cadena.length()) {
            if (Character.toLowerCase(cadena.charAt(i))==v&&pila.empty()) {pila.push(vocal); auxiliar++;}
            else if (Character.toLowerCase(cadena.charAt(i))==v&&!pila.empty()) {pila.pop();}
            i++;
        }
        if (auxiliar==0) {return 0;} else { if (pila.empty()) {return 1;} else {return -1;}}     
   }
        
    }
